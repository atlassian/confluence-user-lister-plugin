package it.com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.people.Group;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.test.rpc.api.permissions.GlobalPermission;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.GroupFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import com.atlassian.confluence.webdriver.pageobjects.page.NoOpPage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.pageobjects.elements.GlobalElementFinder;
import com.atlassian.pageobjects.elements.PageElement;
import org.apache.commons.lang.StringEscapeUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.GroupFixture.groupFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.lang.String.format;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.cssSelector;

@RunWith(ConfluenceStatelessTestRunner.class)
public class UserListerStatelessTest {

    @Inject
    private static ConfluenceTestedProduct product;
    @Inject
    private static ConfluenceRestClient restClient;
    @Inject
    private static ConfluenceRpcClient rpcClient;
    @Inject
    private static GlobalElementFinder finder;

    @Fixture
    private static GroupFixture emptyGroup = groupFixture().build();
    @Fixture
    private static GroupFixture groupOne = groupFixture().build();
    @Fixture
    private static GroupFixture groupTwo = groupFixture().build();

    @Fixture
    private static UserFixture user = userFixture()
            .group(groupOne)
            .group(groupTwo)
            .build();
    @Fixture
    private static UserFixture admin = userFixture()
            .globalPermission(GlobalPermission.CONFLUENCE_ADMIN)
            .build();
    @Fixture
    private static UserFixture userTwoOnly = userFixture()
            .group(groupTwo)
            .build();

    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .permission(userTwoOnly, REGULAR_PERMISSIONS)
            .build();

    @BeforeClass
    public static void init() {
        // cover the nullUserEmailNotShown case
        rpcClient.getAdminSession()
                .getUserComponent()
                .editUser(userTwoOnly.get().getUsername(), userTwoOnly.get().getFullName(), null);
        rpcClient.getAdminSession()
                .getFunctestComponent()
                .disableWebSudo();
    }

    @AfterClass
    public static void teardown() {
        rpcClient.getAdminSession()
                .getFunctestComponent()
                .enableWebSudo();
    }

    @After
    public void logOut() {
        product.logOutFast();
    }

    @Test
    public void testEmptyGroupMessageDisplayedWhenEmptyGroupSpecified() {
        Content emptyGroupPage = restClient.createSession(user.get()).contentService().create(
                Content.builder()
                        .space(space.get())
                        .type(ContentType.PAGE)
                        .title("Empty Group Page")
                        .body(format("{userlister:groups=%s}", emptyGroup.get().getName()), ContentRepresentation.WIKI)
                        .build()
        );

        ViewPage viewPage = product.loginAndView(user.get(), emptyGroupPage);
        waitUntil(
                viewPage.getRenderedContent().getTextTimed(),
                containsString("No results were found for groups : " + emptyGroup.get().getName())
        );
    }

    @Test
    public void testUserRepeatedInGroupListsIfTheyAreMemberOfThoseGroups() {
        Content groupsPage = restClient.createSession(user.get()).contentService().create(
                Content.builder()
                        .space(space.get())
                        .type(ContentType.PAGE)
                        .title("Repeated Group Page")
                        .body(format("{userlister:groups=%s,%s}", groupOne.get().getName(), groupTwo.get().getName()), ContentRepresentation.WIKI)
                        .build()
        );

        product.loginAndView(user.get(), groupsPage);
        assertTableValues(
                "userlister_group_" + StringEscapeUtils.escapeHtml(groupOne.get().getName()),
                format("Group: %s", groupOne.get().getName()),
                format("%s (%s)%n%s", user.get().getFullName(), user.get().getUsername(), user.get().getEmail())
        );
        assertTableValues(
                "userlister_group_" + StringEscapeUtils.escapeHtml(groupTwo.get().getName()),
                format("Group: %s", groupTwo.get().getName()),
                format("%s (%s)%n%s", user.get().getFullName(), user.get().getUsername(), user.get().getEmail()),
                format("%s (%s)", userTwoOnly.get().getFullName(), userTwoOnly.get().getUsername())
        );
    }

    // CONF-16644
    @Test
    public void testXSSinGroupParamWithNoUserInGroup() {
        final String groupName = "<script>alert('Vulnerable')</script>";
        Content xssGroupPage = restClient.createSession(user.get()).contentService().create(
                Content.builder()
                        .space(space.get())
                        .type(ContentType.PAGE)
                        .title("XSS Empty Group Page")
                        .body(format("{userlister:groups=%s}", groupName), ContentRepresentation.WIKI)
                        .build()
        );

        ViewPage viewPage = product.loginAndView(user.get(), xssGroupPage);
        waitUntil(
                viewPage.getRenderedContent().getTextTimed(),
                containsString("No results were found for groups : " + groupName)
        );
    }

    @Test
    public void testXSSinGroupParamWithUsersInGroup() {
        final String groupName = "<script>alert('oops')</script>";
        rpcClient.getAdminSession().getUserComponent().createGroup(groupName);

        List<Group> groups = rpcClient.getAdminSession().getUserComponent().getGroups();
        assertTrue(groups.stream().anyMatch(item -> groupName.equals(item.getName())));

        rpcClient.getAdminSession().getUserComponent().addUserToGroup(user.get().getUsername(), groupName);

        Content xssGroupPage = restClient.createSession(user.get()).contentService().create(
                Content.builder()
                        .space(space.get())
                        .type(ContentType.PAGE)
                        .title("XSS Group Page")
                        .body(format("{userlister:groups=%s}", groupName), ContentRepresentation.WIKI)
                        .build()
        );

        ViewPage viewPage = product.loginAndView(user.get(), xssGroupPage);
        waitUntil(
                viewPage.getRenderedContent().getTextTimed(),
                allOf(
                        containsString(format("Group: %s", groupName)),
                        containsString(format("%s (%s)%n%s", user.get().getFullName(), user.get().getUsername(), user.get().getEmail()))
                )
        );
    }

    @Test
    public void testBlacklistedGroupsDroppedFromListing() {
        ConfigureUserListerPage userListerPage = product.login(admin.get(), ConfigureUserListerPage.class);
        userListerPage.setEntry(groupOne.get().getName());
        userListerPage.save();

        Content groupsPage = restClient.createSession(user.get()).contentService().create(
                Content.builder()
                        .space(space.get())
                        .type(ContentType.PAGE)
                        .title("Blacklist Group Page")
                        .body("{userlister:groups=*}", ContentRepresentation.WIKI)
                        .build()
        );
        product.logOutFast(); // Force logout

        ViewPage viewPage = product.loginAndView(user.get(), groupsPage);

        assertTableValues(
                "userlister_group_" + StringEscapeUtils.escapeHtml(groupTwo.get().getName()),
                format("Group: %s", groupTwo.get().getName()),
                format("%s (%s)%n%s", user.get().getFullName(), user.get().getUsername(), user.get().getEmail()),
                format("%s (%s)", userTwoOnly.get().getFullName(), userTwoOnly.get().getUsername())
        );

        assertTableNotPresent(
                "userlister_group_" + StringEscapeUtils.escapeHtml(groupOne.get().getName())
        );

        waitUntil(
                viewPage.getRenderedContent().getTextTimed(),
                containsString("The output you're seeing is filtered. It contains blocklisted group(s) below.")
        );

        waitUntilTrue(finder.find(By.id("userlister_denied_group_list")).timed().isPresent());
    }

    @Test
    public void testBlacklistedGroupsDroppedFromListingAndWarningSuppressed() {
        ConfigureUserListerPage userListerPage = product.login(admin.get(), ConfigureUserListerPage.class);
        userListerPage.setEntry(groupOne.get().getName());
        userListerPage.save();

        Content groupsPage = restClient.createSession(user.get()).contentService().create(
                Content.builder()
                        .space(space.get())
                        .type(ContentType.PAGE)
                        .title("Blacklist Group With Warning Page")
                        .body("{userlister:groups=*|showWarning=false}", ContentRepresentation.WIKI)
                        .build()
        );

        product.logOutFast(); // Force logout as admin
        ViewPage viewPage = product.loginAndView(user.get(), groupsPage);

        /* Listing of group two should return user and userTwo */
        assertTableValues(
                "userlister_group_" + StringEscapeUtils.escapeHtml(groupTwo.get().getName()),
                format("Group: %s", groupTwo.get().getName()),
                format("%s (%s)%n%s", user.get().getFullName(), user.get().getUsername(), user.get().getEmail()),
                format("%s (%s)", userTwoOnly.get().getFullName(), userTwoOnly.get().getUsername())
        );

        /* Listing of group one should be filtered */
        assertTableNotPresent(
                "userlister_group_" + StringEscapeUtils.escapeHtml(groupOne.get().getName())
        );

        waitUntil(
                viewPage.getRenderedContent().getTextTimed(),
                not(containsString("The output you're seeing is filtered. It contains blocklisted group(s) below."))
        );

        waitUntilFalse(finder.find(By.id("userlister_denied_group_list")).timed().isPresent());
    }

    private void assertTableNotPresent(String id) {
        List<PageElement> rows = finder.findAll(cssSelector(format("#%s tr", id)));
        assertThat(rows, empty());
    }

    private void assertTableValues(final String id, final String... cells) {
        List<PageElement> rows = finder.findAll(cssSelector(format("#%s tr", id)));
        assertThat(rows, hasSize(cells.length));
        int index = 0;
        for (PageElement row : rows) {
            waitUntil(row.timed().getText(), is(cells[index]));
            index++;
        }
    }
}
