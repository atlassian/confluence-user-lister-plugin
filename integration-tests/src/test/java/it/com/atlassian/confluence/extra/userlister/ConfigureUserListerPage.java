package it.com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceAbstractPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static org.hamcrest.Matchers.containsString;

public class ConfigureUserListerPage extends ConfluenceAbstractPage {

    @ElementBy(id = "blackListEntries")
    private PageElement blackListEntries;

    @ElementBy(cssSelector = "input[name=\"save\"]")
    private PageElement saveButton;

    @ElementBy(id = "action-messages")
    private PageElement actionMessages;

    @Override
    public String getUrl() {
        return "/admin/userlister/configure.action";
    }

    public void setEntry(String entry) {
        blackListEntries.clear();
        blackListEntries.type(entry);
    }

    public void save() {
        saveButton.click();
        waitUntil(actionMessages.timed().getText(), containsString("Configuration saved."));
    }
}
