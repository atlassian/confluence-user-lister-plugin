package com.atlassian.confluence.extra.userlister.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.userlister.UserListManager;
import com.atlassian.confluence.extra.userlister.model.UserList;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static com.google.common.base.Joiner.on;

public class ConfigureUserListsAction extends ConfluenceActionSupport {
    private static final Logger logger = LoggerFactory.getLogger(ConfigureUserListsAction.class);

    private String blackListEntries;

    private UserListManager userListManager;

    private UserAccessor userAccessor;

    private String save;

    private Set<String> getBlackListEntriesCollection() throws IOException {
        BufferedReader bufferedReader = null;

        try {
            String line;
            bufferedReader = new BufferedReader(new StringReader(StringUtils.defaultString(blackListEntries)));
            final Set<String> blackListEntries = new TreeSet<>();

            while (null != (line = bufferedReader.readLine())) {
                blackListEntries.add(StringUtils.trim(line));
            }

            return blackListEntries;
        } finally {
            IOUtils.closeQuietly(bufferedReader);
        }
    }

    public String getBlackListEntries() {
        return blackListEntries;
    }

    public void setBlackListEntries(String blackListEntries) {
        this.blackListEntries = blackListEntries;
    }

    public void setUserListManager(UserListManager userListManager) {
        this.userListManager = userListManager;
    }

    @Override
    public void setUserAccessor(UserAccessor userAccessor) {
        super.setUserAccessor(userAccessor);
        this.userAccessor = userAccessor;
    }

    public void setSave(String save) {
        this.save = save;
    }

    @Override
    public String doDefault() {
        setBlackListEntries(StringUtils.join(userListManager.getGroupBlackList(), "\n"));
        return SUCCESS;
    }

    @Override
    public String execute() throws Exception {
        if (StringUtils.equals(getText("add.name"), save)) {
            userListManager.saveGroupBlackList(getBlackListEntriesCollection());

            addActionMessage(getText("userlister.configure.successful"));
            return SUCCESS;
        }

        return CANCEL;
    }

    @Override
    public void validate() {

        try {
            final Collection<String> encodedInvalidGroupNames = getBlackListEntriesCollection().stream()
                    .filter(name -> !StringUtils.equals(UserList.ALL_GROUP_NAME, name) && null == userAccessor.getGroup(name))
                    .map(GeneralUtil::htmlEncode)
                    .collect(Collectors.toSet());

            final String joinedEncodedInvalidGroupNames = on(", ").join(encodedInvalidGroupNames);

            if (!encodedInvalidGroupNames.isEmpty()) {
                addActionError(getText("userlister.configure.invalid.group.names"), joinedEncodedInvalidGroupNames);
            }
        } catch (final IOException ioe) {
            logger.error("Unable to perform action validation", ioe);
            addActionError(getText("userlister.configure.ioexception"));
        }
    }


}
