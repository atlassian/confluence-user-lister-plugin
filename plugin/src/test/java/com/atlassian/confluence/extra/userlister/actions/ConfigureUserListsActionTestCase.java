package com.atlassian.confluence.extra.userlister.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.userlister.UserListManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.impl.DefaultGroup;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.google.common.collect.Iterables.getOnlyElement;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigureUserListsActionTestCase {

    @Mock
    private UserAccessor userAccessor;

    @Mock
    private UserListManager userListManager;

    private ConfigureUserListsAction configureUserListsAction;

    @Before
    public void setUp() {
        configureUserListsAction = new ConfigureUserListsAction() {
            public String getText(String key) {
                return key;
            }

            public String getText(String key, Object[] substitutions) {
                return key + ':' + '[' + substitutions + ']';
            }

            public String getText(String key, String[] substitutions) {
                return key + ':' + '[' + substitutions + ']';
            }
        };
        configureUserListsAction.setUserAccessor(userAccessor);
        configureUserListsAction.setUserListManager(userListManager);
    }

    @Test
    public void testBlackListedGroupsPopulatedAfterDoDefault() {
        final Set<String> blacklistedGroups = new HashSet<>(asList("confluence-users", "confluence-groups"));

        when(userListManager.getGroupBlackList()).thenReturn(blacklistedGroups);

        assertEquals(ConfluenceActionSupport.SUCCESS, configureUserListsAction.doDefault());
        assertTrue(
                ArrayUtils.isEquals(
                        blacklistedGroups.toArray(new String[0]),
                        StringUtils.split(configureUserListsAction.getBlackListEntries(), "\n")
                )
        );
    }

    @Test
    public void testActionResultIsCancelIfSavePropertyIsBlank() throws Exception {
        assertEquals(ConfluenceActionSupport.CANCEL, configureUserListsAction.execute());
    }

    @Test
    public void testBlacklistedGroupsSavedIfSavePropertyIsNotBlank() throws Exception {
        final String[] blacklistedGroupsArray = {"confluence-users", "confluence-groups"};
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");
        final Collection actionMessages;

        configureUserListsAction.setSave("add.name");
        configureUserListsAction.setBlackListEntries(blacklistedEntries);

        assertEquals(ConfluenceActionSupport.SUCCESS, configureUserListsAction.execute());

        verify(userListManager).saveGroupBlackList(
                new HashSet<>(asList(blacklistedGroupsArray))
        );

        actionMessages = configureUserListsAction.getActionMessages();
        assertNotNull(actionMessages);
        assertEquals(1, actionMessages.size());
        assertEquals("userlister.configure.successful", actionMessages.iterator().next());
    }

    @Test
    public void testValidationNotPerformedIfCancelPropertyIsNotBlank() {
        configureUserListsAction.setCancel("back.name");
        configureUserListsAction.validate();

        assertFalse(configureUserListsAction.hasErrors());
    }

    @Test
    public void testValidationPassesWhenAsteriskIsSpecifiedAsOneBlacklistEntry() {
        final String asterisk = "*";
        final String[] blacklistedGroupsArray = {asterisk};
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");


        configureUserListsAction.setBlackListEntries(blacklistedEntries);
        configureUserListsAction.validate();

        verify(userAccessor, never()).getGroup(asterisk);

        assertFalse(configureUserListsAction.hasErrors());
    }

    @Test
    public void testValidationFailsWhenAnInvalidGroupIsSpecifiedAsOneBlacklistEntry() {
        final String invalidGroupName = "invalid-group";
        final String[] blacklistedGroupsArray = {invalidGroupName};
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");

        configureUserListsAction.setBlackListEntries(blacklistedEntries);
        configureUserListsAction.validate();

        assertTrue(configureUserListsAction.hasErrors());
        final Iterable<String> actionErrors = configureUserListsAction.getActionErrors();

        assertNotNull(actionErrors);
        assertTrue(getOnlyElement(actionErrors).startsWith("userlister.configure.invalid.group.names"));
    }

    @Test
    public void testValidationFailsWhenAtLeastOneBlacklistEntryIsInvalid() {
        final String invalidGroupName = "invalid-group";
        final String invalidGroupName2 = "invalid-group2";
        final String validGroupName = "confluence-users";
        final String validGroupName2 = "confluence-administrators";
        final String[] blacklistedGroupsArray = {invalidGroupName, invalidGroupName2, validGroupName, validGroupName2};
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");

        when(userAccessor.getGroup(anyString())).thenAnswer(
                invocationOnMock -> {
                    String arg = invocationOnMock.getArguments()[0].toString();
                    if (arg.equals(validGroupName) || arg.equals(validGroupName2)) {
                        return new DefaultGroup(arg);
                    } else {
                        return null;
                    }
                }
        );

        configureUserListsAction.setBlackListEntries(blacklistedEntries);
        configureUserListsAction.validate();

        assertTrue(configureUserListsAction.hasErrors());

        final Iterable<String> actionErrors = configureUserListsAction.getActionErrors();

        assertNotNull(actionErrors);
        assertTrue(getOnlyElement(actionErrors).startsWith("userlister.configure.invalid.group.names"));
    }

    @Test
    public void testErrorMessageIsEncoded() {
        final String invalidGroupName = "<script>alert(document.cookie);</script>";
        final String[] blacklistedGroupsArray = {invalidGroupName};
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");

        configureUserListsAction.setBlackListEntries(blacklistedEntries);
        configureUserListsAction.validate();

        assertTrue(configureUserListsAction.hasErrors());
        final Iterable<String> actionErrors = configureUserListsAction.getActionErrors();

        assertNotNull(actionErrors);
        assertTrue(getOnlyElement(actionErrors).startsWith("userlister.configure.invalid.group.names"));
        assertFalse(getOnlyElement(actionErrors).contains(invalidGroupName));
    }
}
