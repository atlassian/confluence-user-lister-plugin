package com.atlassian.confluence.extra.userlister;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.confluence.extra.userlister.UserListManager.BANDANA_KEY_BLACK_LIST;
import static com.atlassian.confluence.extra.userlister.model.UserList.ALL_GROUP_NAME;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultUserListManagerTestCase {

    @Mock
    private BandanaManager bandanaManager;

    private CacheManager cacheManager = new MemoryCacheManager();
    private Cache<String, Set<String>> cache;
    private DefaultUserListManager defaultUserListManager;

    @Before
    public void setUp() {
        defaultUserListManager = new DefaultUserListManager(bandanaManager, cacheManager);
        cache = cacheManager.getCache(DefaultUserListManager.class.getName());
    }

    @Test
    public void testBlacklistedGroupsListReturnedNotNullWhenBandanaReturnsNull() {
        final Set<String> blacklistedGroups = defaultUserListManager.getGroupBlackList();
        assertThat(blacklistedGroups, empty());
    }

    @Test
    public void testBlacklistedGroupsListReturnedAccordingToValueSavedInBandana() {
        final Collection<String> blackListedGroups = asList("confluence-users", "confluence-administrators");

        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, BANDANA_KEY_BLACK_LIST))
                .thenReturn(new HashSet<>(blackListedGroups));

        assertThat(
                defaultUserListManager.getGroupBlackList(),
                is(new HashSet<>(blackListedGroups))
        );
    }

    @Test
    public void testBlacklistedGroupsSavedIntoBandana() {
        final Collection<String> blackListedGroups = asList("confluence-users", "confluence-administrators");

        defaultUserListManager.saveGroupBlackList(new HashSet<>(blackListedGroups));
        verify(bandanaManager).setValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                BANDANA_KEY_BLACK_LIST,
                new HashSet<>(blackListedGroups)
        );
    }

    @Test
    public void testAsteriskInBlacklistedGroupsOnlyDeniesWildcards() {
        final Collection<String> blackListedGroups = singletonList(ALL_GROUP_NAME);

        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, BANDANA_KEY_BLACK_LIST))
                .thenReturn(new HashSet<>(blackListedGroups));

        assertFalse(defaultUserListManager.isGroupPermitted(ALL_GROUP_NAME));
        assertTrue(defaultUserListManager.isGroupPermitted("confluence-users"));
    }

    @Test
    public void testBlankGroupNamesAlwaysPermitted() {
        assertTrue(defaultUserListManager.isGroupPermitted("  "));
        assertTrue(defaultUserListManager.isGroupPermitted(EMPTY));
    }

    @Test
    public void testRegisterLoggedInUser() {
        final String sessionId = "1";
        final String userName = "admin";

        defaultUserListManager.registerLoggedInUser(userName, sessionId);

        assertThat(cache.get(userName), is(singleton(sessionId)));
    }

    @Test
    public void testUnregisterLoggedInUser() {
        String sessionId = "1";
        String userName = "admin";

        defaultUserListManager.registerLoggedInUser(userName, sessionId);
        assertThat(cache.get(userName), is(singleton(sessionId)));

        defaultUserListManager.unregisterLoggedInUser(userName, sessionId);
        assertThat(cache.get(userName), is(nullValue()));
    }

    @Test
    public void testUnregisterLoggedInUserWithExistingSession() {
        String sessionId = "1";
        String secondSessionId = "2";
        String userName = "admin";

        defaultUserListManager.registerLoggedInUser(userName, sessionId);
        defaultUserListManager.registerLoggedInUser(userName, secondSessionId);

        assertThat(cache.get(userName), contains(sessionId, secondSessionId));

        defaultUserListManager.unregisterLoggedInUser(userName, secondSessionId);
        assertThat(cache.get(userName), is(singleton(sessionId)));
    }

    @Test
    public void testGetLoggedInUserNames() {
        String userName = "admin";
        String userName2 = "jdoe";

        defaultUserListManager.registerLoggedInUser(userName, "1");
        defaultUserListManager.registerLoggedInUser(userName2, "2");

        assertThat(defaultUserListManager.getLoggedInUsers(), containsInAnyOrder(userName, userName2));
    }
}
